# Change Log
All notable changes to this project will be documented in this file.
This project adheres to [Semantic Versioning](http://semver.org/).

## 0.4.0
### Added
- BILIs

### Removed
- Scrolling background

### Fixed
- Mobile view
- Navigation links
- Spelling errors

## 0.3.0
### Added
- Projects article

## 0.2.0
### Added
- Scrolling background
- Floating articles

### Changed
- Links open new tab
- HTTP to HTTPS where possible

## 0.1.0
### Added
- Old site
